﻿rtdl.CategoricalFeatureTokenizer
================================

.. currentmodule:: rtdl

.. autoclass:: CategoricalFeatureTokenizer
   :special-members: __init__
   :members: