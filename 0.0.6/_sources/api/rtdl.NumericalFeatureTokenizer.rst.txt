﻿rtdl.NumericalFeatureTokenizer
==============================

.. currentmodule:: rtdl

.. autoclass:: NumericalFeatureTokenizer
   :special-members: __init__
   :members: