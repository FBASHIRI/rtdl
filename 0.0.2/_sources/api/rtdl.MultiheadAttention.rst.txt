﻿rtdl.MultiheadAttention
=======================

.. currentmodule:: rtdl

.. autoclass:: MultiheadAttention
   :special-members: __init__
   :members: