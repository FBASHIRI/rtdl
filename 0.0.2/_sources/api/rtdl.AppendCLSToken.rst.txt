﻿rtdl.AppendCLSToken
===================

.. currentmodule:: rtdl

.. autoclass:: AppendCLSToken
   :special-members: __init__
   :members: